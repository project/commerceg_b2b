<?php

namespace Drupal\commerceg_b2b\MachineName\Routing;

/**
 * Holds IDs of routes.
 *
 * @link https://github.com/krystalcode/drupal8-coding-standards/blob/master/Fields.md#field-name-constants
 */
class Route {

  /**
   * Holds the name of the route that lists all B2B organizations.
   */
  const ORGANIZATIONS = 'view.commerceg_organizations.page_1';

  /**
   * Holds the ID of the route that lists all a group's B2B organizations.
   */
  const GROUP_ORGANIZATIONS = 'view.commerceg_group_organizations.page_1';

}
