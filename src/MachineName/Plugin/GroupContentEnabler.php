<?php

namespace Drupal\commerceg_b2b\MachineName\Plugin;

/**
 * Holds machine names of Group Content Enabler plugins.
 *
 * @link https://github.com/krystalcode/drupal8-coding-standards/blob/master/Fields.md#field-name-constants
 */
class GroupContentEnabler {

  /**
   * Holds the machine name for the Group Membership enabler plugin.
   */
  const MEMBERSHIP = 'group_membership';

  /**
   * Holds the machine name for the Organization enabler plugin.
   */
  const ORGANIZATION = 'commerceg_organization';

}
