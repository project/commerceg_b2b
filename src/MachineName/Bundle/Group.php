<?php

namespace Drupal\commerceg_b2b\MachineName\Bundle;

/**
 * Holds machine names of Group entity bundles.
 *
 * @link https://github.com/krystalcode/drupal8-coding-standards/blob/master/Fields.md#field-name-constants
 */
class Group {

  /**
   * Holds the machine name for the Organization group type.
   */
  const ORGANIZATION = 'commerceg_organization';

}
