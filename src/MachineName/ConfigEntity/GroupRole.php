<?php

namespace Drupal\commerceg_b2b\MachineName\ConfigEntity;

/**
 * Holds IDs of Group Role entities.
 *
 * @link https://github.com/krystalcode/drupal8-coding-standards/blob/master/Fields.md#field-name-constants
 */
class GroupRole {

  /**
   * Holds the ID of the anonymous organization role.
   */
  const ANONYMOUS = 'commerceg_organization-anonymous';

  /**
   * Holds the ID of the customer group role.
   *
   * By default, the purchaser role allows organization members to place orders
   * on behalf of the organization. Non-purchaser members will be able to create
   * carts but only purchaser members will be able to go through the checkout
   * process.
   */
  const PURCHASER = 'commerceg_organization-purchaser';

  /**
   * Holds the ID of the group role given to organization manager users.
   *
   * By default, organization manager users will be able to manage other user
   * memberships in the organization.
   */
  const MANAGER = 'commerceg_organization-manager';

  /**
   * Holds the ID of the member organization role.
   */
  const MEMBER = 'commerceg_organization-member';

  /**
   * Holds the ID of the outsider organization role.
   */
  const OUTSIDER = 'commerceg_organization-outsider';

}
