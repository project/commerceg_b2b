<?php

namespace Drupal\commerceg_b2b_organization\Configure;

use Drupal\commerceg\Configure\RoleInstallerTrait;
use Drupal\commerceg_b2b\MachineName\Bundle\Group as GroupBundle;
use Drupal\commerceg_b2b\MachineName\ConfigEntity\GroupRole;
use Drupal\commerceg_b2b\MachineName\Plugin\GroupContentEnabler;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Default implementation of the B2B Organization installer.
 *
 * Provides certain default configuration that makes sense for most B2B
 * scenarios. Applications can override it to provide its own configuration
 * required.
 */
class Installer implements InstallerInterface {

  use RoleInstallerTrait;

  /**
   * The group content storage.
   *
   * @var \Drupal\group\Entity\Storage\GroupContentStorageInterface
   */
  protected $contentStorage;

  /**
   * The group content type storage.
   *
   * @var \Drupal\group\Entity\Storage\GroupContentTypeStorageInterface
   */
  protected $contentTypeStorage;

  /**
   * The group type storage.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityStorageInterface
   */
  protected $groupTypeStorage;

  /**
   * Constructs a new Installer object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->contentStorage = $entity_type_manager->getStorage('group_content');
    $this->groupTypeStorage = $entity_type_manager->getStorage('group_type');

    $this->contentTypeStorage = $entity_type_manager
      ->getStorage('group_content_type');

    // Injections required by the `RoleInstallerTrait` trait.
    $this->groupRoleStorage = $entity_type_manager->getStorage('group_role');
  }

  /**
   * {@inheritdoc}
   */
  public function install() {
    $this->installGroupType();
    $this->installDefaultRoles();
  }

  /**
   * {@inheritdoc}
   */
  public function installGroupType() {
    // Do nothing if the group type already exists. This can happen if the
    // module is uninstalled and then installed again.
    $group_type = $this->groupTypeStorage->load(GroupBundle::ORGANIZATION);
    if ($group_type) {
      return;
    }

    $group_type = $this->groupTypeStorage->create([
      'id' => GroupBundle::ORGANIZATION,
      'label' => 'Organization',
      'description' => 'An organization e.g. a company, non-profit or governmental organization.',
    ]);
    $group_type->save();
  }

  /**
   * {@inheritdoc}
   */
  public function installOrganizationHierarchy() {
    if ($this->isOrganizationHierarchyInstalled()) {
      return;
    }

    $group_type = $this->groupTypeStorage->load(GroupBundle::ORGANIZATION);
    $this->contentTypeStorage
      ->createFromPlugin(
        $group_type,
        GroupContentEnabler::ORGANIZATION
      )
      ->save();
  }

  /**
   * {@inheritdoc}
   */
  public function uninstallOrganizationHierarchy() {
    $content_type = $this->getOrganizationHierarchyContentType();
    $content_type->delete();
  }

  /**
   * {@inheritdoc}
   */
  public function isOrganizationHierarchyInstalled() {
    $content_type = $this->getOrganizationHierarchyContentType();
    if ($content_type) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function canUninstallOrganizationHierarchy() {
    $content_type = $this->getOrganizationHierarchyContentType();

    // We cannot uninstall the group content type if there is group content for
    // it.
    $count = $this->contentStorage
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('type', $content_type->id())
      ->count()
      ->execute();

    return $count ? FALSE : TRUE;
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultRolesInfo() {
    return [
      GroupRole::MANAGER => [
        'id' => GroupRole::MANAGER,
        'label' => 'Manager',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultRolesGroupTypeId() {
    return GroupBundle::ORGANIZATION;
  }

  /**
   * Returns the content type for the organization hierarchy.
   *
   * @return \Drupal\group\Entity\GroupContentInterface|null
   *   The group content type, or NULL if organization hierarchy is not
   *   installed.
   */
  protected function getOrganizationHierarchyContentType() {
    $content_types = $this->contentTypeStorage
      ->loadByContentPluginId(GroupContentEnabler::ORGANIZATION);

    foreach ($content_types as $content_type) {
      if ($content_type->getGroupType()->id() !== GroupBundle::ORGANIZATION) {
        continue;
      }

      return $content_type;
    }
  }

}
