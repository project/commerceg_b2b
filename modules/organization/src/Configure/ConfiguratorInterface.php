<?php

namespace Drupal\commerceg_b2b_organization\Configure;

use Drupal\commerceg\Configure\RoleConfiguratorInterface;

/**
 * Configures the application as expected by B2B Organization functionality.
 *
 * Commerce B2B heavily relies on Commerce Group to provide base integration
 * between Commerce and Group. It then builds on top of certain configuration
 * e.g. an Organization group type with group roles and content installed on it
 * so that it can offer the required functionality.
 *
 * The installer and the configurator services make it easy to prepare such
 * configuration.
 *
 * Note that the expected behavior of the Configurator is to throw exceptions
 * when configuration entities or settings being configured do not exist. This
 * enforces to first run the Installer before configuring.
 *
 * @I Add `diff` method that reports deviations from expected configuration
 *   type     : improvement
 *   priority : normal
 *   labels   : 1.0.0, config, organization
 */
interface ConfiguratorInterface extends RoleConfiguratorInterface {

  /**
   * Configures all Organization-related settings.
   *
   * It includes:
   *   - Configures the Organization group type.
   */
  public function configure();

  /**
   * Configures the Organization group type.
   *
   * @throws \RuntimeException
   *   If the group type is not installed.
   */
  public function configureGroupType();

  /**
   * Configures organization hierarchy.
   *
   * It includes:
   *   - Configures the corresponding group content enabler plugin.
   *
   * @throws \RuntimeException
   *   If organization hierarchy is not enabled.
   */
  public function configureOrganizationHierarchy();

  /**
   * Configures the shopping context settings.
   *
   * When `$enable_personal_context` is TRUE, it makes sure that shopping
   * context is enabled and that the shopping context group type is the
   * Organization.
   *
   * When `$enable_personal_context` is FALSE, it disables shopping context.
   *
   * @param bool $enable_personal_context
   *   Whether personal shopping context should be enabled or not.
   */
  public function configureContext($enable_personal_context);

  /**
   * Enables multiple organization memberships per user.
   */
  public function enableMultipleMemberships();

  /**
   * Disables multiple organization memberships per user.
   */
  public function disableMultipleMemberships();

  /**
   * Returns whether multiple organization memberships per user are enabled.
   *
   * @return bool
   *   TRUE if multiple memberships are enabled, FALSE otherwise.
   */
  public function areMultipleMembershipsEnabled();

  /**
   * Returns whether multiple organization memberships per user can be disabled.
   *
   * By default, this feature should not be disabled if there are already users
   * with multiple memberships. Other conditions may be added by specific
   * customizations.
   *
   * @return bool
   *   TRUE if multiple memberships can be disabled, FALSE otherwise.
   */
  public function canDisableMultipleMemberships();

}
