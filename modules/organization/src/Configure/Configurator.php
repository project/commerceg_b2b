<?php

namespace Drupal\commerceg_b2b_organization\Configure;

use Drupal\commerceg\Configure\RoleConfiguratorTrait;
use Drupal\commerceg_b2b\MachineName\Bundle\Group as GroupBundle;
use Drupal\commerceg_b2b\MachineName\ConfigEntity\GroupRole;
use Drupal\commerceg_b2b\MachineName\Plugin\GroupContentEnabler;

use Drupal\group\Plugin\GroupContentEnablerManagerInterface;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection as DatabaseConnection;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Default implementation of the B2B Organization configurator.
 *
 * Provides certain default configuration that makes sense for most B2B
 * scenarios. Applications can override it to provide its own configuration
 * required.
 */
class Configurator implements ConfiguratorInterface {

  use RoleConfiguratorTrait;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The group content entity type.
   *
   * @var \Drupal\Core\Entity\EntityTypeInterface
   */
  protected $contentEntityType;

  /**
   * The group content type storage.
   *
   * @var \Drupal\group\Entity\Storage\GroupContentTypeStorageInterface
   */
  protected $contentTypeStorage;

  /**
   * The group type storage.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityStorageInterface
   */
  protected $groupTypeStorage;

  /**
   * The shopping context configuration.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $contextConfig;

  /**
   * Constructs a new Configurator object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\group\Plugin\GroupContentEnablerManagerInterface $content_enabler_manager
   *   The group content enabler plugin manager.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    DatabaseConnection $database,
    EntityFieldManagerInterface $entity_field_manager,
    EntityTypeManagerInterface $entity_type_manager,
    GroupContentEnablerManagerInterface $content_enabler_manager
  ) {
    $this->database = $database;
    $this->entityFieldManager = $entity_field_manager;
    $this->groupTypeStorage = $entity_type_manager->getStorage('group_type');

    $this->contextConfig = $config_factory
      ->getEditable('commerceg_context.settings');
    $this->contentEntityType = $entity_type_manager
      ->getDefinition('group_content');
    $this->contentTypeStorage = $entity_type_manager
      ->getStorage('group_content_type');

    // Injections required by the `RoleConfiguratorTrait` trait.
    $this->contentEnablerManager = $content_enabler_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->groupRoleStorage = $entity_type_manager->getStorage('group_role');
  }

  /**
   * {@inheritdoc}
   */
  public function configure() {
    $this->configureGroupType();
    $this->configureDefaultRoles();
    $this->configureContext();
  }

  /**
   * {@inheritdoc}
   */
  public function configureGroupType() {
    $group_type = $this->groupTypeStorage->load(GroupBundle::ORGANIZATION);
    if (!$group_type) {
      throw new \RuntimeException(
        'Cannot configure a group type that does not exist.'
      );
    }

    $group_type->set('creator_membership', FALSE);
    $group_type->set('creator_wizard', FALSE);
    $group_type->save();

    $this->configureGroupTypeTitleFieldLabel();
  }

  /**
   * {@inheritdoc}
   */
  public function configureContext($enable_personal_context = TRUE) {
    $this->contextConfig
      ->set('status', TRUE)
      ->set('personal_context.status', $enable_personal_context)
      ->set('group_context.group_type', GroupBundle::ORGANIZATION)
      ->save();
  }

  /**
   * {@inheritdoc}
   */
  public function enableMultipleMemberships() {
    $this->configureMembership(TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function disableMultipleMemberships() {
    $this->configureMembership(FALSE);
  }

  /**
   * {@inheritdoc}
   */
  public function areMultipleMembershipsEnabled() {
    $configuration = $this->getMembershipContentType()
      ->getContentPlugin()
      ->getConfiguration();

    return $configuration['group_cardinality'] == 1 ? FALSE : TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function canDisableMultipleMemberships() {
    $content_type = $this->getMembershipContentType();
    $table = $this->contentEntityType->getDataTable();

    $results = $this->database->query(
      'SELECT COUNT(*) AS count FROM (
          SELECT COUNT(*) AS count FROM {' . $table . '}
          WHERE `type` = :type
          GROUP BY `entity_id` HAVING count > 1
      ) AS gc',
      [':type' => $content_type->id()]
    )->fetchAll();

    return $results[0]->count ? FALSE : TRUE;
  }

  /**
   * Configures the title field label for the Organization group type.
   *
   * `Name` makes more sense than `Title` for an organization.
   */
  protected function configureGroupTypeTitleFieldLabel() {
    $title_field = $this->entityFieldManager
      ->getFieldDefinitions('group', GroupBundle::ORGANIZATION)
      ['label'];

    $title_field
      ->getConfig(GroupBundle::ORGANIZATION)
      ->setLabel('Name')
      ->save();
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultRolesInfo() {
    return [
      GroupRole::ANONYMOUS => [
        'id' => GroupRole::ANONYMOUS,
        'label' => 'Anonymous',
      ],
      GroupRole::MANAGER => [
        'id' => GroupRole::MANAGER,
        'label' => 'Manager',
      ],
      GroupRole::MEMBER => [
        'id' => GroupRole::MEMBER,
        'label' => 'Member',
      ],
      GroupRole::OUTSIDER => [
        'id' => GroupRole::OUTSIDER,
        'label' => 'Outsider',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultRolePermissions() {
    return [
      GroupRole::ANONYMOUS => [
        // Group permissions.
        'access content overview' => FALSE,
        'administer group' => FALSE,
        'delete group' => FALSE,
        'edit group' => FALSE,
        'view group' => FALSE,
        // Group membership permissions.
        'administer members' => FALSE,
        'update own group_membership content' => FALSE,
        'join group' => FALSE,
        'leave group' => FALSE,
        'view group_membership content' => FALSE,
        // Child organization permissions.
        'access commerceg_organization overview' => FALSE,
        'create commerceg_organization entity' => FALSE,
        'delete any commerceg_organization entity' => FALSE,
        'delete own commerceg_organization entity' => FALSE,
        'update any commerceg_organization entity' => FALSE,
        'update own commerceg_organization entity' => FALSE,
        'view commerceg_organization entity' => FALSE,
        'create commerceg_organization content' => FALSE,
        'delete any commerceg_organization content' => FALSE,
        'delete own commerceg_organization content' => FALSE,
        'update any commerceg_organization content' => FALSE,
        'update own commerceg_organization content' => FALSE,
        'view commerceg_organization content' => FALSE,
      ],
      GroupRole::OUTSIDER => [
        // Group permissions.
        'access content overview' => FALSE,
        'administer group' => FALSE,
        'delete group' => FALSE,
        'edit group' => FALSE,
        'view group' => FALSE,
        // Group membership permissions.
        'administer members' => FALSE,
        'update own group_membership content' => FALSE,
        'join group' => FALSE,
        'leave group' => FALSE,
        'view group_membership content' => FALSE,
        // Child organization permissions.
        'access commerceg_organization overview' => FALSE,
        'create commerceg_organization entity' => FALSE,
        'delete any commerceg_organization entity' => FALSE,
        'delete own commerceg_organization entity' => FALSE,
        'update any commerceg_organization entity' => FALSE,
        'update own commerceg_organization entity' => FALSE,
        'view commerceg_organization entity' => FALSE,
        'create commerceg_organization content' => FALSE,
        'delete any commerceg_organization content' => FALSE,
        'delete own commerceg_organization content' => FALSE,
        'update any commerceg_organization content' => FALSE,
        'update own commerceg_organization content' => FALSE,
        'view commerceg_organization content' => FALSE,
      ],
      GroupRole::MEMBER => [
        // Group permissions.
        'access content overview' => FALSE,
        'administer group' => FALSE,
        'delete group' => FALSE,
        'edit group' => FALSE,
        'view group' => TRUE,
        // Group membership permissions.
        'administer members' => FALSE,
        'update own group_membership content' => TRUE,
        'join group' => FALSE,
        'leave group' => FALSE,
        'view group_membership content' => TRUE,
        // Child organization permissions.
        'access commerceg_organization overview' => TRUE,
        'create commerceg_organization entity' => FALSE,
        'delete any commerceg_organization entity' => FALSE,
        'delete own commerceg_organization entity' => FALSE,
        'update any commerceg_organization entity' => FALSE,
        'update own commerceg_organization entity' => FALSE,
        'view commerceg_organization entity' => TRUE,
        'create commerceg_organization content' => FALSE,
        'delete any commerceg_organization content' => FALSE,
        'delete own commerceg_organization content' => FALSE,
        'update any commerceg_organization content' => FALSE,
        'update own commerceg_organization content' => FALSE,
        'view commerceg_organization content' => TRUE,
      ],
      GroupRole::MANAGER => [
        // Group permissions.
        'access content overview' => FALSE,
        'administer group' => FALSE,
        'delete group' => FALSE,
        'edit group' => TRUE,
        'view group' => NULL,
        // Group membership permissions.
        'administer members' => TRUE,
        'update own group_membership content' => NULL,
        'join group' => NULL,
        'leave group' => NULL,
        'view group_membership content' => NULL,
        // Child organization permissions.
        'access commerceg_organization overview' => NULL,
        'create commerceg_organization entity' => FALSE,
        'delete any commerceg_organization entity' => FALSE,
        'delete own commerceg_organization entity' => FALSE,
        'update any commerceg_organization entity' => TRUE,
        'update own commerceg_organization entity' => NULL,
        'view commerceg_organization entity' => NULL,
        'create commerceg_organization content' => FALSE,
        'delete any commerceg_organization content' => FALSE,
        'delete own commerceg_organization content' => FALSE,
        'update any commerceg_organization content' => TRUE,
        'update own commerceg_organization content' => NULL,
        'view commerceg_organization content' => NULL,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function configureOrganizationHierarchy() {
    $content_type = $this->getOrganizationHierarchyContentType();

    // There should only be one content type since we don't have derivatives.
    if (!$content_type) {
      throw new \RuntimeException(
        'Cannot configure a plugin that is not installed.'
      );
    }

    $content_type->updateContentPlugin([
      // We assume that it only makes sense that an organization has one parent
      // only.
      'group_cardinality' => 1,
      // We assume that it only makes sense that an organization has only one
      // group content on the same parent organization. If that's not the case a
      // different group content enabler plugin is likely to be used for the
      // specific use case.
      'entity_cardinality' => 1,
      // We assume that the group content type does not have additional
      // fields; it is therefore not necessary to use the wizard.
      'use_creation_wizard' => 0,
    ]);
    $content_type->save();
  }

  /**
   * Returns the content type for the organization hierarchy.
   *
   * @return \Drupal\group\Entity\GroupContentInterface|null
   *   The group content type, or NULL if organization hierarchy is not
   *   installed.
   */
  protected function getOrganizationHierarchyContentType() {
    $content_types = $this->contentTypeStorage
      ->loadByContentPluginId(GroupContentEnabler::ORGANIZATION);

    foreach ($content_types as $content_type) {
      if ($content_type->getGroupType()->id() !== GroupBundle::ORGANIZATION) {
        continue;
      }

      return $content_type;
    }
  }

  /**
   * Configures the content type defining organization memberships.
   *
   * @param bool $allow_multiple
   *   Whether multiple organization memberships should be allowed or not.
   */
  protected function configureMembership($allow_multiple = TRUE) {
    $content_type = $this->getMembershipContentType();

    // There should only be one content type since we don't have derivatives.
    if (!$content_type) {
      throw new \RuntimeException(
        'Cannot configure a plugin that is not installed.'
      );
    }

    $content_type->updateContentPlugin([
      // Allow a user to be member of multiple organizations or not, as
      // required.
      'group_cardinality' => $allow_multiple ? 0 : 1,
      // We assume that it only makes sense that a user has only one group
      // content on the same organization. If that's not the case a different
      // group content enabler plugin is likely to be used for the specific use
      // case.
      'entity_cardinality' => 1,
      // We assume that the group content type does not have additional
      // fields; it is therefore not necessary to use the wizard.
      'use_creation_wizard' => 0,
    ]);
    $content_type->save();
  }

  /**
   * Returns the content type for the organization membership.
   *
   * @return \Drupal\group\Entity\GroupContentInterface|null
   *   The group content type, or NULL if it is not found.
   */
  protected function getMembershipContentType() {
    $content_types = $this->contentTypeStorage
      ->loadByContentPluginId(GroupContentEnabler::MEMBERSHIP);

    foreach ($content_types as $content_type) {
      if ($content_type->getGroupType()->id() !== GroupBundle::ORGANIZATION) {
        continue;
      }

      return $content_type;
    }
  }

}
