<?php

namespace Drupal\commerceg_b2b_organization\Configure\Form;

use Drupal\commerceg\Form\ConfigFormBase;
use Drupal\commerceg\MachineName\Config\PersonalContextDisabledMode as DisabledModeConfig;
use Drupal\commerceg_b2b_organization\Configure\ConfiguratorInterface;
use Drupal\commerceg_b2b_organization\Configure\InstallerInterface;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Routing\RedirectDestinationInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\TranslationInterface;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Configure B2B organization-related settings.
 */
class Settings extends ConfigFormBase {

  /**
   * The B2B organization configurator.
   *
   * @var \Drupal\commerceg_b2b_organization\Configure\ConfiguratorInterface
   */
  protected $configurator;

  /**
   * The B2B organization installer.
   *
   * @var \Drupal\commerceg_b2b_organization\Configure\InstallerInterface
   */
  protected $installer;

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger channel factory.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\Core\Routing\RedirectDestinationInterface $redirect_destination
   *   The redirect destination service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   * @param \Drupal\commerceg_b2b_organization\Configure\ConfiguratorInterface $configurator
   *   The B2B organization configurator.
   * @param \Drupal\commerceg_b2b_organization\Configure\InstallerInterface $installer
   *   The B2B organization installer.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    LoggerChannelFactoryInterface $logger_factory,
    MessengerInterface $messenger,
    RedirectDestinationInterface $redirect_destination,
    RequestStack $request_stack,
    RouteMatchInterface $route_match,
    TranslationInterface $string_translation,
    ConfiguratorInterface $configurator,
    InstallerInterface $installer
  ) {
    parent::__construct(
      $config_factory,
      $logger_factory,
      $messenger,
      $redirect_destination,
      $request_stack,
      $route_match,
      $string_translation
    );

    $this->configurator = $configurator;
    $this->installer = $installer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('logger.factory'),
      $container->get('messenger'),
      $container->get('redirect.destination'),
      $container->get('request_stack'),
      $container->get('current_route_match'),
      $container->get('string_translation'),
      $container->get('commerceg_b2b_organization.configurator'),
      $container->get('commerceg_b2b_organization.installer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'commerceg_b2b_organization_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'commerceg_context.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = $this->buildHierarchyForm($form, $form_state);
    $form = $this->buildMembershipForm($form, $form_state);
    $form = $this->buildPersonalContextForm($form, $form_state);

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $this->validateHierarchyForm($form, $form_state);
    $this->validateMembershipForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->submitHierarchyForm($form, $form_state);
    $this->submitMembershipForm($form, $form_state);
    $this->submitPersonalContextForm($form, $form_state);

    $form_state->setRedirect('commerceg_b2b.config');

    parent::submitForm($form, $form_state);
  }

  /**
   * Builds the form related to organization hierarchy.
   *
   * @param array $form
   *   The form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The updated form render array.
   */
  protected function buildHierarchyForm(
    array $form,
    FormStateInterface $form_state
  ) {
    $description = $this->t(
      'Enabling organization hierarchy will allow organizations to be registered
       as children of other organizations.'
    );

    $disabled = FALSE;
    $is_installed = $this->installer->isOrganizationHierarchyInstalled();
    $can_uninstall = NULL;
    if ($is_installed) {
      $can_uninstall = $this->installer->canUninstallOrganizationHierarchy();
    }

    if ($is_installed && !$can_uninstall) {
      $disabled = TRUE;
      $description .= '<p><em>' . $this->t(
        'Organization hierarchy cannot be uninstalled because there are
         organizations using it. You must detach all children organizations
         from their parents or remove them before proceeding.'
      ) . '</em></p>';
    }

    $form['hierarchy_status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable organization hierarchy'),
      '#description' => $description,
      '#default_value' => $is_installed,
      '#disabled' => $disabled,
    ];

    return $form;
  }

  /**
   * Builds the form related to organization membership.
   *
   * @param array $form
   *   The form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The updated form render array.
   */
  protected function buildMembershipForm(
    array $form,
    FormStateInterface $form_state
  ) {
    $description = $this->t(
      'Allowing users to be members of more than one organization will allow
       them to use a single email to log in (personal or organization-provided
       email). When they log in they will be able to choose on behalf of which
       organization they are currently acting, and they will only see the
       relevant product, content, prices, carts etc.'
    );

    $disabled = FALSE;
    $is_enabled = $this->configurator->areMultipleMembershipsEnabled();
    $can_disable = NULL;
    if ($is_enabled) {
      $can_disable = $this->configurator->canDisableMultipleMemberships();
    }

    if ($is_enabled && !$can_disable) {
      $disabled = TRUE;
      $description .= '<p><em>' . $this->t(
        'Multiple memberships per user cannot be disabled because there are
         already users that have a membership to more than one organization.
         You must review and ensure that each user is member of one organization
         only before proceeding.'
      ) . '</em></p>';
    }

    $form['membership_mode'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow multiple organization memberships per user'),
      '#description' => $description,
      '#default_value' => $is_enabled,
      '#disabled' => $disabled,
    ];

    return $form;
  }

  /**
   * Builds the form related to personal context settings.
   *
   * Here we replicate settings that are made available by the Commerce Group
   * Context module because we want to provide a better B2B configuration
   * experience and have all related settings at one obvious configuration page
   * or section.
   *
   * We also provide better form element titles/description with the knowledge
   * that the shopping context is an organization group.
   *
   * @param array $form
   *   The form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The updated form render array.
   *
   * @I Display status for disabling personal shopping context
   *    type     : bug
   *    priority : low
   *    labels   : context
   *    notes    : Review the consequences of disabling personal shopping
   *               context when there are already personal carts. Either block
   *               disabling or provide informational text/instructions for
   *               preventing problems.
   */
  protected function buildPersonalContextForm(
    array $form,
    FormStateInterface $form_state
  ) {
    $settings = $this->config('commerceg_context.settings')
      ->get('personal_context');

    $form['personal_context'] = [
      '#type' => 'details',
      '#title' => $this->t('Personal shopping context'),
      '#open' => TRUE,
      '#tree' => TRUE,
    ];

    $form['personal_context']['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable personal shopping context'),
      '#description' => $this->t(
        'Enabling personal shopping context will permit users to make purchases
         for themselves i.e. outside of the context of an organization.'
      ),
      '#default_value' => $settings['status'],
    ];

    $form['personal_context']['disabled_mode']['mode'] = [
      '#type' => 'select',
      '#title' => $this->t('Disabled mode'),
      '#description' => $this->t(
        'When the personal shopping context is disabled and the "Disable" mode
         is selected, any UI elements that allow users to create carts (such as
         the "Add to cart" button on product pages) will remain visible but they
         will be disabled if the user is not acting within the context of an
         organization. If the "Hide" mode is selected, the UI elements will be
         hidden instead.'
      ),
      '#default_value' => $settings['disabled_mode']['mode'],
      '#options' => [
        DisabledModeConfig::MODE_DISABLE => $this->t('Disable'),
        DisabledModeConfig::MODE_HIDE => $this->t('Hide'),
      ],
      '#states' => [
        'visible' => [
          ':input[data-drupal-selector="edit-personal-context-status"]' => [
            'checked' => FALSE,
          ],
        ],
      ],
    ];

    $form['personal_context']['disabled_mode']['add_to_cart_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Disabled mode message'),
      '#description' => $this->t(
        'Optionally, a message that will be displayed on the "Add to cart" form.
         If the "Disabled" mode is selected, the message will be displayed next
         to the "Add to cart" button; if the "Hide" mode is selected the message
         will be displayed instead of the "Add to cart" button.'
      ),
      '#default_value' => $settings['disabled_mode']['add_to_cart_message'],
      '#states' => [
        'visible' => [
          ':input[data-drupal-selector="edit-personal-context-status"]' => [
            'checked' => FALSE,
          ],
        ],
      ],
    ];

    return $form;
  }

  /**
   * Validates the form related to organization hierarchy.
   *
   * @param array $form
   *   The form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  protected function validateHierarchyForm(
    array &$form,
    FormStateInterface $form_state
  ) {
    $values = $form_state->getValues();
    $is_installed = $this->installer->isOrganizationHierarchyInstalled();
    $can_uninstall = NULL;
    if ($is_installed) {
      $can_uninstall = $this->installer->canUninstallOrganizationHierarchy();
    }

    if (!$values['hierarchy_status'] && $is_installed && !$can_uninstall) {
      $form_state->setErrorByName(
        'hierarchy_status',
        $this->t(
          'Organization hierarchy cannot be uninstalled because there are
           organizations using it. You must detach all children organizations
           from their parents or remove them before proceeding.'
        )
      );
    }
  }

  /**
   * Validates the form related to organization membership.
   *
   * @param array $form
   *   The form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  protected function validateMembershipForm(
    array &$form,
    FormStateInterface $form_state
  ) {
    $values = $form_state->getValues();
    $is_enabled = $this->configurator->areMultipleMembershipsEnabled();
    $can_disable = NULL;
    if ($is_enabled) {
      $can_disable = $this->configurator->canDisableMultipleMemberships();
    }

    if (!$values['membership_mode'] && $is_enabled && !$can_disable) {
      $form_state->setErrorByName(
        'membership_mode',
        $this->t(
          'Multiple memberships per user cannot be disabled because there are
           already users that have a membership to more than one organization.
           You must review and ensure that each user is member of one
           organization only before proceeding.'
        )
      );
    }
  }

  /**
   * Submits the form related to organization hierarchy.
   *
   * @param array $form
   *   The form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  protected function submitHierarchyForm(
    array &$form,
    FormStateInterface $form_state
  ) {
    $values = $form_state->getValues();
    $is_installed = $this->installer->isOrganizationHierarchyInstalled();

    if ($values['hierarchy_status'] && !$is_installed) {
      $this->installer->installOrganizationHierarchy();
      $this->configurator->configureOrganizationHierarchy();
      $this->messenger()->addStatus(
        $this->t('Organization hierarchy has been successfully installed.')
      );
    }
    elseif (!$values['hierarchy_status'] && $is_installed) {
      $this->installer->uninstallOrganizationHierarchy();
      $this->messenger()->addStatus(
        $this->t('Organization hierarchy has been successfully uninstalled.')
      );
    }
  }

  /**
   * Submits the form related to organization membership.
   *
   * @param array $form
   *   The form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  protected function submitMembershipForm(
    array &$form,
    FormStateInterface $form_state
  ) {
    $values = $form_state->getValues();
    $is_enabled = $this->configurator->areMultipleMembershipsEnabled();

    if ($values['membership_mode'] && !$is_enabled) {
      $this->configurator->enableMultipleMemberships();
      $this->messenger()->addStatus(
        $this->t('Multiple memberships have been successfully enabled.')
      );
    }
    elseif (!$values['membership_mode'] && $is_enabled) {
      $this->configurator->disableMultipleMemberships();
      $this->messenger()->addStatus(
        $this->t('Multiple memberships have been successfully disabled.')
      );
    }
  }

  /**
   * Submit actions related to personal context settings.
   *
   * @param array $form
   *   The form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function submitPersonalContextForm(
    array &$form,
    FormStateInterface $form_state
  ) {
    $config = $this->config('commerceg_context.settings');
    $values = $form_state->getValues();

    if ($values['personal_context']['status']) {
      $config->set('personal_context.status', TRUE)->save();
      return;
    }

    $config
      ->set('personal_context.status', FALSE)
      ->set(
        'personal_context.disabled_mode.mode',
        $values['personal_context']['disabled_mode']['mode']
      )
      ->set(
        'personal_context.disabled_mode.add_to_cart_message',
        $values['personal_context']['disabled_mode']['add_to_cart_message']
      )
      ->save();
  }

}
