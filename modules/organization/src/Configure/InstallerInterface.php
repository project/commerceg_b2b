<?php

namespace Drupal\commerceg_b2b_organization\Configure;

use Drupal\commerceg\Configure\RoleInstallerInterface;

/**
 * Installs configuration as expected by B2B Organization functionality.
 *
 * Commerce B2B heavily relies on Commerce Group to provide base integration
 * between Commerce and Group. It then builds on top of certain configuration
 * e.g. an Organization group type with group roles and content installed on it
 * so that it can offer the required functionality.
 *
 * The installer and the configurator services make it easy to prepare such
 * configuration.
 *
 * Note that the expected behavior of the Installer is to install expected
 * configuration if it doesn't exist, but to not throw exceptions if it is
 * already installed. That way, it can be safely run again as additions are
 * being made without errors on previously installed configuration. Uninstalling
 * is different though; exceptions should be thrown when trying to uninstall
 * something that is not installed.
 *
 * @I Add `diff` method that reports deviations from expected configuration
 *   type     : improvement
 *   priority : normal
 *   labels   : 1.0.0, config, organization
 *
 * @I Add method for installing the views as well
 *   type     : improvement
 *   priority : normal
 *   labels   : 1.0.0, config, organization
 */
interface InstallerInterface extends RoleInstallerInterface {

  /**
   * Installs all Organization-related configuration.
   *
   * It includes:
   *   - Installing the Organization group type.
   */
  public function install();

  /**
   * Installs the Organization group type, if it doesn't exist.
   */
  public function installGroupType();

  /**
   * Installs hierarchy for organizations.
   *
   * It includes:
   *   - Installing the group content type that enables adding organizations to
   *     groups.
   */
  public function installOrganizationHierarchy();

  /**
   * Uninstalls hierarchy for organizations.
   *
   * It includes:
   *   - Uninstalling the group content type that enables adding organizations
   *     to groups.
   */
  public function uninstallOrganizationHierarchy();

  /**
   * Returns whether hierarchy for organizations can be uninstalled.
   */
  public function canUninstallOrganizationHierarchy();

  /**
   * Returns whether hierarchy for organizations is enabled.
   *
   * @return bool
   *   TRUE if organization hierarchy is installed i.e. integration enabled,
   *   FALSE otherwise.
   *
   * @see \Drupal\commerceg_b2b_organization\Configure\InstallerInterface::installOrganizationHierarchy()
   */
  public function isOrganizationHierarchyInstalled();

}
