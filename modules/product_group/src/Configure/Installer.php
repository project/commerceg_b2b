<?php

namespace Drupal\commerceg_b2b_product_group\Configure;

use Drupal\commerceg_b2b\MachineName\Bundle\Group as GroupBundle;
use Drupal\commerceg_product_group\Configure\InstallerInterface as BaseInstallerInterface;

use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Default implementation of the B2B Product Group installer.
 */
class Installer implements InstallerInterface {

  /**
   * The Product Group installer.
   *
   * @var \Drupal\commerceg_product_group\Configure\InstallerInterface
   */
  protected $baseInstaller;

  /**
   * The group type storage.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityStorageInterface
   */
  protected $groupTypeStorage;

  /**
   * Constructs a new Installer object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerceg_product_group\Configure\InstallerInterface $base_installer
   *   The Product Group installer.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    BaseInstallerInterface $base_installer
  ) {
    $this->baseInstaller = $base_installer;

    $this->groupTypeStorage = $entity_type_manager
      ->getStorage('group_type');
  }

  /**
   * {@inheritdoc}
   */
  public function install() {
    $this->installProductGroupContentType();
    $this->baseInstaller->installManagedMembershipRoles();
  }

  /**
   * {@inheritdoc}
   */
  public function installProductGroupContentType() {
    $group_type = $this->groupTypeStorage->load(GroupBundle::ORGANIZATION);
    $this->baseInstaller->installProductGroupContentType($group_type);
  }

}
