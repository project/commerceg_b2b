<?php

namespace Drupal\commerceg_b2b_product_group\Configure;

/**
 * Installs configuration as expected by B2B Product Group functionality.
 *
 * Commerce B2B Product Group heavily relies on Commerce Group to provide base
 * integration between Commerce and Group, and Commerce Product Group to provide
 * base functionality related to product groups. It then builds on top of
 * certain configuration e.g. a group content type that enables associations
 * between organizations and product groups, so that it can offer the required
 * functionality.
 *
 * The installer and the configurator services make it easy to prepare such
 * configuration.
 *
 * Note that the expected behavior of the Installer is to install expected
 * configuration if it doesn't exist, but to not throw exceptions if it is
 * already installed. That way, it can be safely run again as additions are
 * being made without errors on previously installed configuration. Uninstalling
 * is different though; exceptions should be thrown when trying to uninstall
 * something that is not installed.
 *
 * @I Add `diff` method that reports deviations from expected configuration
 *   type     : improvement
 *   priority : normal
 *   labels   : 1.0.0, config
 */
interface InstallerInterface {

  /**
   * Installs all product group configuration related to organizations.
   *
   * It includes:
   *   - Installing a group content type for associating product groups with
   *     organizations.
   *   - Installs the Group Customer group role on product groups, as required
   *     for the managed memberships feature.
   */
  public function install();

  /**
   * Installs a content type for associationg product groups with organizations.
   */
  public function installProductGroupContentType();

}
