<?php

namespace Drupal\commerceg_b2b_product_group\Configure;

use Drupal\commerceg\Configure\RoleConfiguratorTrait;
use Drupal\commerceg_b2b\MachineName\Bundle\Group as GroupBundle;
use Drupal\commerceg_product_group\Configure\ConfiguratorInterface as BaseConfiguratorInterface;
use Drupal\commerceg_b2b\MachineName\ConfigEntity\GroupRole;

use Drupal\group\Plugin\GroupContentEnablerManagerInterface;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Default implementation of the B2B Product Group configurator.
 */
class Configurator implements ConfiguratorInterface {

  use RoleConfiguratorTrait;

  /**
   * The Product Group configurator.
   *
   * @var \Drupal\commerceg_b2b_product_group\Configure\ConfiguratorInterface
   */
  protected $baseConfigurator;

  /**
   * The group type storage.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityStorageInterface
   */
  protected $groupTypeStorage;

  /**
   * The Product Group module configuration.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $productGroupConfig;

  /**
   * Constructs a new Configurator object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerceg_product_group\Configure\ConfiguratorInterface $base_configurator
   *   The Product Group configurator.
   * @param \Drupal\group\Plugin\GroupContentEnablerManagerInterface $content_enabler_manager
   *   The group content enabler plugin manager.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    EntityTypeManagerInterface $entity_type_manager,
    BaseConfiguratorInterface $base_configurator,
    GroupContentEnablerManagerInterface $content_enabler_manager
  ) {
    $this->baseConfigurator = $base_configurator;
    $this->groupTypeStorage = $entity_type_manager->getStorage('group_type');

    $this->productGroupConfig = $config_factory
      ->getEditable('commerceg_product_group.settings');

    // Injections required by the `RoleConfiguratorTrait` trait.
    $this->contentEnablerManager = $content_enabler_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->groupRoleStorage = $entity_type_manager->getStorage('group_role');
  }

  /**
   * {@inheritdoc}
   */
  public function configure() {
    $this->configureManagedMemberships();
    $this->configureProductGroupContentType();
    $this->configureDefaultRoles();
  }

  /**
   * {@inheritdoc}
   */
  public function configureProductGroupContentType() {
    $group_type = $this->groupTypeStorage->load(GroupBundle::ORGANIZATION);
    $this->baseConfigurator->configureProductGroupContentType($group_type);
  }

  /**
   * {@inheritdoc}
   */
  public function configureManagedMemberships() {
    $group_type = $this->groupTypeStorage->load(GroupBundle::ORGANIZATION);
    if (!$group_type) {
      throw new \RuntimeException(sprintf(
        'Cannot configure managed memberships for organizations because the %s group type does not exist.',
        GroupBundle::ORGANIZATION
      ));
    }

    $changed = FALSE;

    // Enable the managed membership feature.
    if (!$this->productGroupConfig->get('managed_memberships.status')) {
      $this->productGroupConfig->set('managed_memberships.status', TRUE);
      $changed = TRUE;
    }

    // Add the Organization group type to the managed membership group types.
    $group_types = $this->productGroupConfig
      ->get('managed_memberships.group_types');
    $group_types = $group_types ?? [];
    if (!in_array(GroupBundle::ORGANIZATION, $group_types)) {
      $group_types[] = GroupBundle::ORGANIZATION;
      $this->productGroupConfig->set(
        'managed_memberships.group_types',
        $group_types
      );
      $changed = TRUE;
    }

    if ($changed) {
      $this->productGroupConfig->save();
    }

    // Configure group permissions for managed membership roles.
    $this->baseConfigurator->configureManagedMembershipRoles();
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultRolesInfo() {
    return [
      GroupRole::ANONYMOUS => [
        'id' => GroupRole::ANONYMOUS,
        'label' => 'Anonymous',
      ],
      GroupRole::MANAGER => [
        'id' => GroupRole::MANAGER,
        'label' => 'Manager',
      ],
      GroupRole::MEMBER => [
        'id' => GroupRole::MEMBER,
        'label' => 'Member',
      ],
      GroupRole::OUTSIDER => [
        'id' => GroupRole::OUTSIDER,
        'label' => 'Outsider',
      ],
      // @I Configure Purchaser role only if the Order submodule is installed
      //    type     : bug
      //    priority : low
      //    labels   : 1.0.0, configurator, role
      GroupRole::PURCHASER => [
        'id' => GroupRole::PURCHASER,
        'label' => 'Purchaser',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultRolePermissions() {
    // No group member has access to product groups. These should be managed by
    // site administrator users and can be configured at the advanced outsider
    // permissions.
    return [
      GroupRole::ANONYMOUS => [
        // Product group permissions.
        'access commerceg_product_group overview' => FALSE,
        'create commerceg_products content' => FALSE,
        'delete any commerceg_products content' => FALSE,
        'delete own commerceg_products content' => FALSE,
        'update any commerceg_products content' => FALSE,
        'update own commerceg_products content' => FALSE,
        'view commerceg_products content' => FALSE,
      ],
      GroupRole::OUTSIDER => [
        // Product group permissions.
        'access commerceg_product_group overview' => FALSE,
        'create commerceg_products content' => FALSE,
        'delete any commerceg_products content' => FALSE,
        'delete own commerceg_products content' => FALSE,
        'update any commerceg_products content' => FALSE,
        'update own commerceg_products content' => FALSE,
        'view commerceg_products content' => FALSE,
      ],
      GroupRole::MEMBER => [
        // Product group permissions.
        'access commerceg_product_group overview' => FALSE,
        'create commerceg_products content' => FALSE,
        'delete any commerceg_products content' => FALSE,
        'delete own commerceg_products content' => FALSE,
        'update any commerceg_products content' => FALSE,
        'update own commerceg_products content' => FALSE,
        'view commerceg_products content' => FALSE,
      ],
      GroupRole::MANAGER => [
        // Product group permissions.
        'access commerceg_product_group overview' => FALSE,
        'create commerceg_products content' => FALSE,
        'delete any commerceg_products content' => FALSE,
        'delete own commerceg_products content' => FALSE,
        'update any commerceg_products content' => FALSE,
        'update own commerceg_products content' => FALSE,
        'view commerceg_products content' => FALSE,
      ],
      GroupRole::PURCHASER => [
        // Product group permissions.
        'access commerceg_product_group overview' => FALSE,
        'create commerceg_products content' => FALSE,
        'delete any commerceg_products content' => FALSE,
        'delete own commerceg_products content' => FALSE,
        'update any commerceg_products content' => FALSE,
        'update own commerceg_products content' => FALSE,
        'view commerceg_products content' => FALSE,
      ],
    ];
  }

}
