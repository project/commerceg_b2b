<?php

namespace Drupal\commerceg_b2b_product_group\Configure;

use Drupal\commerceg\Configure\RoleConfiguratorInterface;

/**
 * Configures the application as expected by B2B Product Group functionality.
 *
 * Commerce B2B Product Group heavily relies on Commerce Group to provide base
 * integration between Commerce and Group, and Commerce Product Group to provide
 * base functionality related to product groups. It then builds on top of
 * certain configuration e.g. a group content type that enables associations
 * between organizations and product groups, so that it can offer the required
 * functionality.
 *
 * The installer and the configurator services make it easy to prepare such
 * configuration.
 *
 * Note that the expected behavior of the Configurator is to throw exceptions
 * when configuration entities or settings being configured do not exist. This
 * enforces to first run the Installer before configuring.
 *
 * @I Add `diff` method that reports deviations from expected configuration
 *   type     : improvement
 *   priority : normal
 *   labels   : 1.0.0, config
 */
interface ConfiguratorInterface extends RoleConfiguratorInterface {

  /**
   * Configures all product group settings related to organizations.
   *
   * It includes:
   *   - Configures the group content type that allows associating product
   *     groups with organizations.
   *   - Enables managed memberships for organizations.
   */
  public function configure();

  /**
   * Configures the content type for product group-to-organization associations.
   *
   * @throws \RuntimeException
   *   If the group content type is not installed for the given group type.
   */
  public function configureProductGroupContentType();

  /**
   * Enables and configures the managed memberships feature for organizations.
   *
   * By default, it includes the following:
   *   - Enables the managed memberships features.
   *   - Adds the Organization group type to the managed membership group types.
   *   - Configures the permissions for the managed membership roles.
   *
   * @throws \RuntimeException
   *   If the organization group type is not installed.
   */
  public function configureManagedMemberships();

}
