<?php

namespace Drupal\commerceg_b2b_order\Hook;

use Drupal\commerceg_b2b_order\Configure\ConfiguratorInterface;
use Drupal\commerceg_b2b_order\Configure\InstallerInterface;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;

/**
 * Holds methods implementing form alteration hooks.
 */
class FormAlter {

  use StringTranslationTrait;

  /**
   * The B2B order module configurator.
   *
   * @var \Drupal\commerceg_b2b_order\Configure\ConfiguratorInterface
   */
  protected $configurator;

  /**
   * The B2B order module installer.
   *
   * @var \Drupal\commerceg_b2b_order\Configure\InstallerInterface
   */
  protected $installer;

  /**
   * Constructs a new FormAlter object.
   *
   * @param \Drupal\commerceg_b2b_order\Configure\ConfiguratorInterface $configurator
   *   The B2B order configurator.
   * @param \Drupal\commerceg_b2b_order\Configure\InstallerInterface $installer
   *   The B2B order installer.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   */
  public function __construct(
    ConfiguratorInterface $configurator,
    InstallerInterface $installer,
    TranslationInterface $string_translation
  ) {
    $this->installer = $installer;
    $this->configurator = $configurator;
    $this->stringTranslation = $string_translation;
  }

  /**
   * Implements hook_form_FORM_ID_alter().
   *
   * Handles B2B integration settings for order types.
   *
   * @param array $form
   *   The form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function orderTypeFormAlter(
    array &$form,
    FormStateInterface $form_state
  ) {
    $order_type = $form_state->getFormObject()->getEntity();

    $description = $this->t(
      'When enabled, users will be able to place orders of this type on behalf
       of their organizations.'
    );

    $disabled = FALSE;
    $can_uninstall = NULL;
    $is_installed = $this->installer->isOrderTypeInstalled($order_type);
    if ($is_installed) {
      $can_uninstall = $this->installer->canUninstallOrderType($order_type);
    }

    if ($is_installed && !$can_uninstall) {
      $disabled = TRUE;
      $description .= '<p><em>' . $this->t(
        'You cannot remove availability of this order type from Organizations
         because there already exist orders or carts of this type that belong to
         Organizations.'
      ) . '</em></p>';
    }

    $form['commerceg_b2b_status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Make orders available to B2B organizations'),
      '#description' => $description,
      '#default_value' => $is_installed,
      '#disabled' => $disabled,
    ];

    // We do not add validate/submit callbacks in this class to avoid mixing
    // static with non-static methods; that would create issues with writing
    // automated tests.
    $form['actions']['submit']['#validate'][] = '\Drupal\commerceg_b2b_order\Form\Callbacks::orderTypeValidate';
    $form['actions']['submit']['#submit'][] = '\Drupal\commerceg_b2b_order\Form\Callbacks::orderTypeSubmit';
  }

}
