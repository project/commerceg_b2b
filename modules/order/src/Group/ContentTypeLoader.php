<?php

namespace Drupal\commerceg_b2b_order\Group;

use Drupal\commerce_order\Entity\OrderTypeInterface;
use Drupal\commerceg\Group\ContentTypeLoader as ContentTypeLoaderBase;
use Drupal\commerceg\MachineName\Plugin\GroupContentEnabler;
use Drupal\commerceg_b2b\MachineName\Bundle\Group as GroupBundle;

use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Default implementation for the B2B Order content type loader service.
 */
class ContentTypeLoader extends ContentTypeLoaderBase implements
  ContentTypeLoaderInterface {

  /**
   * The group content type storage.
   *
   * @var \Drupal\group\Entity\Storage\GroupContentTypeStorageInterface
   */
  protected $contentTypeStorage;

  /**
   * Constructs a new ContentTypeLoader object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->contentTypeStorage = $entity_type_manager
      ->getStorage('group_content_type');
  }

  /**
   * {@inheritdoc}
   */
  public function loadForOrderType(OrderTypeInterface $order_type) {
    return $this->loadByPluginIdAndGroupTypeId(
      GroupContentEnabler::ORDER . ':' . $order_type->id(),
      GroupBundle::ORGANIZATION
    );
  }

}
