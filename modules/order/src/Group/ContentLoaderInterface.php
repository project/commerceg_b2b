<?php

namespace Drupal\commerceg_b2b_order\Group;

use Drupal\commerce_order\Entity\OrderInterface;

/**
 * Facilitates loading of group content entities related to B2B Orders.
 *
 * @see \Drupal\commerceg\Group\ContentLoaderInterface
 */
interface ContentLoaderInterface {

  /**
   * Returns the Organization group content entities for the given order.
   *
   * There should only be one content that associates the given order with its
   * Organization group.
   *
   * @param Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   *
   * @return \Drupal\group\Entity\GroupContentInterface|null
   *   The group content, or NULL if the given order is not associated with a
   *   B2B Organization.
   *
   * @throws \Drupal\commerceg\Exception\RuntimeConfigurationException
   *   When the group content enabler plugin for the type (bundle) of the given
   *   order is not installed on B2B organizations.
   */
  public function loadForOrder(OrderInterface $order);

}
