<?php

namespace Drupal\commerceg_b2b_order\Group;

use Drupal\commerce_order\Entity\OrderTypeInterface;

/**
 * Facilitates loading of group content type entities related to B2B Orders.
 *
 * @see \Drupal\commerceg\Group\ContentTypeLoaderInterface
 */
interface ContentTypeLoaderInterface {

  /**
   * Returns the content type for the given order type on Organizations.
   *
   * There should only be one content type that enables the given order type on
   * Organization groups.
   *
   * @param Drupal\commerce_order\Entity\OrderTypeInterface $order_type
   *   The order type.
   *
   * @return \Drupal\group\Entity\GroupContentTypeInterface|null
   *   The group content type, or NULL if the given order type is not
   *   installed on B2B Organizations.
   *
   * @throws \Drupal\commerceg\Exception\RuntimeConfigurationException
   *   When the group content enabler plugin for the given order type is not
   *   installed on the B2B Organization group type.
   */
  public function loadForOrderType(OrderTypeInterface $order_type);

}
