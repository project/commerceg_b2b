<?php

namespace Drupal\commerceg_b2b_order\Group;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerceg\Group\ContentLoader as ContentLoaderBase;
use Drupal\commerceg\MachineName\Plugin\GroupContentEnabler;
use Drupal\commerceg_b2b\MachineName\Bundle\Group as GroupBundle;

/**
 * Default implementation for the B2B Order content loader service.
 */
class ContentLoader extends ContentLoaderBase implements
  ContentLoaderInterface {

  /**
   * {@inheritdoc}
   */
  public function loadForOrder(OrderInterface $order) {
    $contents = $this->loadForEntityByPluginIdAndGroupTypeId(
      $order,
      GroupContentEnabler::ORDER . ':' . $order->bundle(),
      GroupBundle::ORGANIZATION
    );

    // `current` would return FALSE when the aren't any content entities in the
    // array. We need to return NULL to follow the interface.
    return $contents ? current($contents) : NULL;
  }

}
