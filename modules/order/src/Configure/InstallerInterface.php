<?php

namespace Drupal\commerceg_b2b_order\Configure;

use Drupal\commerce_order\Entity\OrderTypeInterface;
use Drupal\commerceg\Configure\RoleInstallerInterface;

/**
 * Installs configuration as expected by B2B Order functionality.
 *
 * Commerce B2B heavily relies on Commerce Group to provide base integration
 * between Commerce and Group. It then builds on top of certain configuration
 * e.g. an Organization group type with order type content enablers installed on
 * it so that it can offer the required functionality.
 *
 * The installer and the configurator services make it easy to prepare such
 * configuration.
 *
 * Note that the expected behavior of the Installer is to install expected
 * configuration if it doesn't exist, but to not throw exceptions if it is
 * already installed. That way, it can be safely run again as additions are
 * being made without errors on previously installed configuration. Uninstalling
 * is different though; exceptions should be thrown when trying to uninstall
 * something that is not installed.
 *
 * @I Add `diff` method that reports deviations from expected configuration
 *   type     : improvement
 *   priority : normal
 *   labels   : 1.0.0, config, order
 */
interface InstallerInterface extends RoleInstallerInterface {

  /**
   * Enables B2B integration for the given order type.
   *
   * It includes:
   *   - Installing the corresponding group content enabler plugin for the
   *     Organization group type.
   *
   * @param Drupal\commerce_order\Entity\OrderTypeInterface $order_type
   *   The order type to install.
   */
  public function installOrderType(OrderTypeInterface $order_type);

  /**
   * Disables B2B integration for the given order type.
   *
   * @param Drupal\commerce_order\Entity\OrderTypeInterface $order_type
   *   The order type to uninstall.
   *
   * @throws \Drupal\commerceg\Exception\RuntimeConfigurationException
   *   When the given order type is not installed on the B2B Organization group
   *   type.
   */
  public function uninstallOrderType(OrderTypeInterface $order_type);

  /**
   * Returns whether B2B integration for the given order type is enabled.
   *
   * @param Drupal\commerce_order\Entity\OrderTypeInterface $order_type
   *   The order type to install.
   *
   * @return bool
   *   TRUE if the order type is installed i.e. integration enabled, FALSE
   *   otherwise.
   *
   * @see \Drupal\commerceg_b2b_order\Configure\InstallerInterface::installOrderType()
   */
  public function isOrderTypeInstalled(OrderTypeInterface $order_type);

  /**
   * Returns whether B2B integration for the given order type can be disabled.
   *
   * @param Drupal\commerce_order\Entity\OrderTypeInterface $order_type
   *   The order type to check.
   *
   * @return bool
   *   TRUE if the order type can be uninstalled i.e. disable integration,
   *   FALSE otherwise.
   *
   * @throws \Drupal\commerceg\Exception\RuntimeConfigurationException
   *   When the given order type is not installed on the B2B Organization group
   *   type.
   */
  public function canUninstallOrderType(OrderTypeInterface $order_type);

}
