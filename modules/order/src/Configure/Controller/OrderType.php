<?php

namespace Drupal\commerceg_b2b_order\Configure\Controller;

use Drupal\Core\Controller\ControllerBase;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFormBuilderInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Routing\RedirectDestinationInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\TranslationInterface;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller for listing order types and their B2B integration status.
 */
class OrderType extends ControllerBase {

  /**
   * Constructs a new OrderType object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user account.
   * @param \Drupal\Core\Entity\EntityFormBuilderInterface $entity_form_builder
   *   The entity form builder.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger channel factory.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Routing\RedirectDestinationInterface $redirect_destination
   *   The redirect destination service.
   * @param \Drupal\Core\State\StateInterface $state
   *   The Drupal state service.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    AccountInterface $current_user,
    EntityFormBuilderInterface $entity_form_builder,
    EntityTypeManagerInterface $entity_type_manager,
    FormBuilderInterface $form_builder,
    LanguageManagerInterface $language_manager,
    LoggerChannelFactoryInterface $logger_factory,
    MessengerInterface $messenger,
    ModuleHandlerInterface $module_handler,
    RedirectDestinationInterface $redirect_destination,
    StateInterface $state,
    TranslationInterface $string_translation
  ) {
    $this->configFactory = $config_factory;
    $this->currentUser = $current_user;
    $this->entityFormBuilder = $entity_form_builder;
    $this->entityTypeManager = $entity_type_manager;
    $this->formBuilder = $form_builder;
    $this->languageManager = $language_manager;
    $this->loggerFactory = $logger_factory;
    $this->messenger = $messenger;
    $this->moduleHandler = $module_handler;
    $this->redirectDestination = $redirect_destination;
    $this->state = $state;
    $this->stringTranslation = $string_translation;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('current_user'),
      $container->get('entity.form_builder'),
      $container->get('entity_type.manager'),
      $container->get('form_builder'),
      $container->get('language_manager'),
      $container->get('logger.factory'),
      $container->get('messenger'),
      $container->get('module_handler'),
      $container->get('redirect.destination'),
      $container->get('state'),
      $container->get('string_translation')
    );
  }

  /**
   * Provides the listing page for order types.
   *
   * @return array
   *   A render array as expected by
   *   \Drupal\Core\Render\RendererInterface::render().
   */
  public function listing() {
    $list_builder = new OrderTypeListBuilder(
      $this->entityTypeManager()->getDefinition('commerce_order_type'),
      $this->entityTypeManager()->getStorage('commerce_order_type'),
      $this->entityTypeManager()
    );
    return $list_builder->render();
  }

}
