<?php

namespace Drupal\commerceg_b2b_order\Configure\Controller;

use Drupal\commerceg\MachineName\Plugin\GroupContentEnabler;
use Drupal\commerceg_b2b\MachineName\Bundle\Group as GroupBundle;

use Drupal\commerce_order\OrderTypeListBuilder as ListBuilderBase;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Url;

/**
 * Provides an order type list builder for the B2B configuration section.
 *
 * @I Add description to order types in Commerce Order, and then here
 *    type     : improvement
 *    priority : low
 *    labels   : config, external, order
 */
class OrderTypeListBuilder extends ListBuilderBase {

  /**
   * The group type storage.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityStorageInterface
   */
  protected $groupTypeStorage;

  /**
   * The group content type storage.
   *
   * @var \Drupal\group\Entity\Storage\GroupContentTypeStorageInterface
   */
  protected $contentTypeStorage;

  /**
   * The list of already B2B-installed order types.
   *
   * Keyed by the order type ID, holds the ID of the group content type enabling
   * the integration.
   *
   * @var array
   */
  protected $installedTypes;

  /**
   * Constructs a new OrderTypeListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    EntityTypeInterface $entity_type,
    EntityStorageInterface $storage,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    parent::__construct($entity_type, $storage);

    $this->groupTypeStorage = $entity_type_manager
      ->getStorage('group_type');
    $this->contentTypeStorage = $entity_type_manager
      ->getStorage('group_content_type');

    $this->buildInstalledTypes();
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Order type');
    $header['id'] = $this->t('ID');
    $header['b2b_status'] = $this->t('B2B integration status');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label']['data'] = [
      '#type' => 'link',
      '#title' => $entity->label(),
      '#url' => $entity->toUrl('edit-form'),
    ];
    $row['id'] = $entity->id();

    if (in_array($entity->id(), array_keys($this->installedTypes))) {
      // @I Include information when the configuration is not the B2B default
      //   type     : improvement
      //   priority : normal
      //   labels   : config, order
      $row['b2b_status'] = $this->t(
        '<em>Installed</em>: orders of this type can be added to organizations.'
      );
    }
    else {
      $row['b2b_status'] = $this->t(
        '<em>Available</em>: orders of this type cannot be added to
         organizations until integration is enabled.'
      );
    }

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getOperations(EntityInterface $entity) {
    $installed = in_array($entity->id(), array_keys($this->installedTypes));

    // Provide a link to install, if the order type is not already installed.
    if (!$installed) {
      return [
        'install' => [
          'title' => $this->t('Install'),
          'url' => Url::fromRoute(
            'commerceg_b2b.config.install_order_type',
            ['commerce_order_type' => $entity->id()]
          ),
        ],
      ];
    }

    // If the order type is already installed, provide links to configure, reset
    // configuration to B2B defaults, and uninstall.
    return [
      // @I Redirect back to the B2B order type configuration page
      //   type     : improvement
      //   priority : low
      //   labels   : config, order
      'configure' => [
        'title' => $this->t('Configure'),
        'url' => Url::fromRoute(
          'entity.group_content_type.edit_form',
          ['group_content_type' => $this->installedTypes[$entity->id()]]
        ),
      ],
      // @I Do not link to reset configuration when it is the B2B default
      //   type     : improvement
      //   priority : low
      //   labels   : config, order
      'reset' => [
        'title' => $this->t('Reset configuration'),
        'url' => Url::fromRoute(
          'commerceg_b2b.config.reset_order_type',
          ['commerce_order_type' => $entity->id()]
        ),
      ],
      'uninstall' => [
        'title' => $this->t('Uninstall'),
        'url' => Url::fromRoute(
          'entity.group_content_type.delete_form',
          ['group_content_type' => $this->installedTypes[$entity->id()]]
        ),
      ],
    ];
  }

  /**
   * Builds the list of already installed order types.
   */
  protected function buildInstalledTypes() {
    $group_type = $this->groupTypeStorage->load(GroupBundle::ORGANIZATION);
    $content_types = $this->contentTypeStorage->loadByGroupType($group_type);
    $base_plugin_id_length = strlen(GroupContentEnabler::ORDER) + 1;

    $this->installedTypes = [];
    foreach ($content_types as $content_type) {
      $plugin_id = $content_type->getContentPluginId();
      if (strpos($plugin_id, GroupContentEnabler::ORDER . ':') === FALSE) {
        continue;
      }

      $order_type_id = substr($plugin_id, $base_plugin_id_length);
      $this->installedTypes[$order_type_id] = $content_type->id();
    }
  }

}
