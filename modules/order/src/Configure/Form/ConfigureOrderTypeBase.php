<?php

namespace Drupal\commerceg_b2b_order\Configure\Form;

use Drupal\commerceg_b2b_order\Configure\ConfiguratorInterface;
use Drupal\commerceg_b2b_order\Configure\InstallerInterface;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Routing\RedirectDestinationInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\TranslationInterface;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Base class for order type configuration forms.
 *
 * Mostly needed for easily injecting all required dependencies.
 */
abstract class ConfigureOrderTypeBase extends FormBase {

  /**
   * The B2B order configurator.
   *
   * @var \Drupal\commerceg_b2b_order\Configure\ConfiguratorInterface
   */
  protected $configurator;

  /**
   * The B2B order installer.
   *
   * @var \Drupal\commerceg_b2b_order\Configure\InstallerInterface
   */
  protected $installer;

  /**
   * The order type.
   *
   * @var \Drupal\commerce_order\Entity\OrderTypeInterface
   */
  protected $orderType;

  /**
   * Constructs a new ResetOrderType object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger channel factory.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\Core\Routing\RedirectDestinationInterface $redirect_destination
   *   The redirect destination service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   * @param \Drupal\commerceg_b2b_order\Configure\ConfiguratorInterface $configurator
   *   The B2B order configurator.
   * @param \Drupal\commerceg_b2b_order\Configure\InstallerInterface $installer
   *   The B2B order installer.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    LoggerChannelFactoryInterface $logger_factory,
    MessengerInterface $messenger,
    RedirectDestinationInterface $redirect_destination,
    RequestStack $request_stack,
    RouteMatchInterface $route_match,
    TranslationInterface $string_translation,
    ConfiguratorInterface $configurator,
    InstallerInterface $installer
  ) {
    $this->configFactory = $config_factory;
    $this->loggerFactory = $logger_factory;
    $this->messenger = $messenger;
    $this->redirectDestination = $redirect_destination;
    $this->requestStack = $request_stack;
    $this->routeMatch = $route_match;
    $this->stringTranslation = $string_translation;
    $this->configurator = $configurator;
    $this->installer = $installer;

    $this->orderType = $this->routeMatch->getParameter('commerce_order_type');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('logger.factory'),
      $container->get('messenger'),
      $container->get('redirect.destination'),
      $container->get('request_stack'),
      $container->get('current_route_match'),
      $container->get('string_translation'),
      $container->get('commerceg_b2b_order.configurator'),
      $container->get('commerceg_b2b_order.installer')
    );
  }

}
