<?php

namespace Drupal\commerceg_b2b_order\Configure\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Form for installing the B2B integration for an order type.
 */
class InstallOrderType extends ConfigureOrderTypeBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'commerceg_b2b_install_order_type';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    if ($this->installer->isOrderTypeInstalled($this->orderType)) {
      return $this->buildInstalledForm($form, $form_state);
    }

    $form['help'] = [
      '#markup' => '<p>' . $this->t(
        'Installing the %label order type on B2B organizations will allow orders
         of this type to be added to such organizations. Are you sure?',
        ['%label' => $this->orderType->label()]
      ) . '</p>',
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Install'),
    ];
    $form['cancel'] = [
      '#type' => 'link',
      '#title' => $this->t('Cancel'),
      '#url' => Url::fromRoute('commerceg_b2b.config.order_types'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->installer->installOrderType($this->orderType);
    $this->configurator->configureOrderType($this->orderType);

    $this->messenger->addStatus(
      $this->t(
        'The %label order type has been successfully installed on B2B
         organizations',
        ['%label' => $this->orderType->label()]
      )
    );

    $form_state->setRedirect('commerceg_b2b.config.order_types');
  }

  /**
   * Builds the form for the case that the order type is installed.
   *
   * We cannot install an already installed order type.
   *
   * @param array $form
   *   The form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return array
   *   The form render array.
   */
  protected function buildInstalledForm(
    array $form,
    FormStateInterface $form_state
  ) {
    $form['help'] = [
      '#markup' => '<p>' . $this->t(
        'The %label order type is already installed on B2B organizations.',
        ['%label' => $this->orderType->label()]
      ) . '</p>',
    ];
    $form['cancel'] = [
      '#type' => 'link',
      '#title' => $this->t('Go back'),
      '#url' => Url::fromRoute('commerceg_b2b.config.order_types'),
    ];

    return $form;
  }

}
