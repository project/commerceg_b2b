<?php

namespace Drupal\commerceg_b2b_order\Configure\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Form for resetting the B2B integration for an order type to the defaults.
 */
class ResetOrderType extends ConfigureOrderTypeBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'commerceg_b2b_reset_order_type';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    if (!$this->installer->isOrderTypeInstalled($this->orderType)) {
      return $this->buildUninstalledForm($form, $form_state);
    }

    // @I Do not reset configuration that is already the B2B defaults
    //    type     : improvement
    //    priority : low
    //    labels   : config, order
    // @I List what the defaults are, and what the changes will be
    //    type     : improvement
    //    priority : normal
    //    labels   : config, order
    $form['help'] = [
      '#markup' => '<p>' . $this->t(
        'Proceeding will reset the configuration for adding %label orders to
         B2B organizations to the defaults provided by Commerce B2B. This
         includes cart and order-related permissions. Are you sure?',
        ['%label' => $this->orderType->label()]
      ) . '</p>',
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Reset'),
    ];
    $form['cancel'] = [
      '#type' => 'link',
      '#title' => $this->t('Cancel'),
      '#url' => Url::fromRoute('commerceg_b2b.config.order_types'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configurator->configureOrderType($this->orderType);

    $this->messenger->addStatus(
      $this->t(
        'The %label order type installation on B2B organizations has been
         successfully reset to the default configuration.',
        ['%label' => $this->orderType->label()]
      )
    );

    $form_state->setRedirect('commerceg_b2b.config.order_types');
  }

  /**
   * Builds the form for the case that the order type is not installed.
   *
   * We cannot configure an order type that does not have B2B integration
   * enabled yet.
   *
   * @param array $form
   *   The form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return array
   *   The form render array.
   */
  protected function buildUninstalledForm(
    array $form,
    FormStateInterface $form_state
  ) {
    $form['help'] = [
      '#markup' => '<p>' . $this->t(
        'The %label order type is not installed on B2B organizations.',
        ['%label' => $this->orderType->label()]
      ) . '</p>',
    ];
    $form['cancel'] = [
      '#type' => 'link',
      '#title' => $this->t('Go back'),
      '#url' => Url::fromRoute('commerceg_b2b.config.order_types'),
    ];

    return $form;
  }

}
