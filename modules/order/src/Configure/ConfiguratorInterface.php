<?php

namespace Drupal\commerceg_b2b_order\Configure;

use Drupal\commerce_order\Entity\OrderTypeInterface;
use Drupal\commerceg\Configure\RoleConfiguratorInterface;

/**
 * Configures the application as expected by B2B Order functionality.
 *
 * Commerce B2B heavily relies on Commerce Group to provide base integration
 * between Commerce and Group. It then builds on top of certain configuration
 * e.g. an Organization group type with order type content enablers installed on
 * it so that it can offer the required functionality.
 *
 * The installer and the configurator services make it easy to prepare such
 * configuration.
 *
 * Note that the expected behavior of the Configurator is to throw exceptions
 * when configuration entities or settings being configured do not exist. This
 * enforces to first run the Installer before configuring.
 *
 * @I Add `diff` method that reports deviations from expected configuration
 *   type     : improvement
 *   priority : normal
 *   labels   : 1.0.0, config, order
 */
interface ConfiguratorInterface extends RoleConfiguratorInterface {

  /**
   * Configures all default order-related settings.
   *
   * It includes:
   *   - Configures the default group roles.
   */
  public function configure();

  /**
   * Configures the B2B integration for the given order type.
   *
   * It includes:
   *   - Configures the corresponding group content enabler plugin.
   *
   * @param Drupal\commerce_order\Entity\OrderTypeInterface $order_type
   *   The order type to configure.
   *
   * @throws \RuntimeException
   *   If the B2B integration for the given order type is not enabled.
   */
  public function configureOrderType(OrderTypeInterface $order_type);

}
