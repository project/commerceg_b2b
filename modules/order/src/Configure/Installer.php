<?php

namespace Drupal\commerceg_b2b_order\Configure;

use Drupal\commerceg\Configure\RoleInstallerTrait;
use Drupal\commerceg\Exception\RuntimeConfigurationException;
use Drupal\commerceg\MachineName\Plugin\GroupContentEnabler;
use Drupal\commerceg_b2b\MachineName\Bundle\Group as GroupBundle;
use Drupal\commerceg_b2b\MachineName\ConfigEntity\GroupRole;
use Drupal\commerceg_b2b_order\Group\ContentTypeLoaderInterface;

use Drupal\commerce_order\Entity\OrderTypeInterface;

use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Default implementation of the B2B Order installer.
 *
 * Provides certain default configuration that makes sense for most B2B
 * scenarios. Applications can override it to provide its own configuration
 * required.
 */
class Installer implements InstallerInterface {

  use RoleInstallerTrait;

  /**
   * The B2B Order content type loader.
   *
   * @var \Drupal\commerceg_b2b_order\Group\ContentTypeLoaderInterface
   */
  protected $contentTypeLoader;

  /**
   * The group content storage.
   *
   * @var \Drupal\group\Entity\Storage\GroupContentStorageInterface
   */
  protected $contentStorage;

  /**
   * The group content type storage.
   *
   * @var \Drupal\group\Entity\Storage\GroupContentTypeStorageInterface
   */
  protected $contentTypeStorage;

  /**
   * The group type storage.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityStorageInterface
   */
  protected $groupTypeStorage;

  /**
   * Constructs a new Installer object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerceg_b2b_order\Group\ContentTypeLoaderInterface $content_type_loader
   *   The B2B Order content type loader.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    ContentTypeLoaderInterface $content_type_loader
  ) {
    $this->contentTypeLoader = $content_type_loader;

    $this->contentStorage = $entity_type_manager->getStorage('group_content');
    $this->groupTypeStorage = $entity_type_manager->getStorage('group_type');

    $this->contentTypeStorage = $entity_type_manager
      ->getStorage('group_content_type');

    // Injections required by the `RoleInstallerTrait` trait.
    $this->groupRoleStorage = $entity_type_manager->getStorage('group_role');
  }

  /**
   * {@inheritdoc}
   */
  public function install() {
    $this->installDefaultRoles();
  }

  /**
   * {@inheritdoc}
   */
  public function installOrderType(OrderTypeInterface $order_type) {
    if ($this->isOrderTypeInstalled($order_type)) {
      return;
    }

    $group_type = $this->groupTypeStorage->load(GroupBundle::ORGANIZATION);
    $this->contentTypeStorage
      ->createFromPlugin(
        $group_type,
        GroupContentEnabler::ORDER . ':' . $order_type->id()
      )
      ->save();
  }

  /**
   * {@inheritdoc}
   */
  public function uninstallOrderType(OrderTypeInterface $order_type) {
    $content_type = $this->contentTypeLoader->loadForOrderType($order_type);
    $this->contentTypeStorage->delete([$content_type]);
  }

  /**
   * {@inheritdoc}
   */
  public function isOrderTypeInstalled(OrderTypeInterface $order_type) {
    // The content type loader throws an exception when loading a content type
    // that is not installed.
    try {
      $this->contentTypeLoader->loadForOrderType($order_type);
    }
    catch (RuntimeConfigurationException $exception) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function canUninstallOrderType(OrderTypeInterface $order_type) {
    $content_type = $this->contentTypeLoader->loadForOrderType($order_type);

    // We cannot uninstall the group content type if there is group content for
    // it.
    $count = $this->contentStorage
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('type', $content_type->id())
      ->count()
      ->execute();

    return $count ? FALSE : TRUE;
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultRolesInfo() {
    return [
      GroupRole::PURCHASER => [
        'id' => GroupRole::PURCHASER,
        'label' => 'Purchaser',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultRolesGroupTypeId() {
    return GroupBundle::ORGANIZATION;
  }

}
