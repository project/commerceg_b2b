<?php

namespace Drupal\commerceg_b2b_order\Configure;

use Drupal\commerceg\Configure\RoleConfiguratorTrait;
use Drupal\commerceg\MachineName\Plugin\GroupContentEnabler;
use Drupal\commerceg_b2b\MachineName\Bundle\Group as GroupBundle;
use Drupal\commerceg_b2b\MachineName\ConfigEntity\GroupRole;

use Drupal\commerce_order\Entity\OrderTypeInterface;
use Drupal\group\Plugin\GroupContentEnablerManagerInterface;

use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Default implementation of the B2B Order configurator.
 *
 * Provides certain default configuration that makes sense for most B2B
 * scenarios. Applications can override it to provide its own configuration
 * required.
 */
class Configurator implements ConfiguratorInterface {

  use RoleConfiguratorTrait;

  /**
   * The group content type storage.
   *
   * @var \Drupal\group\Entity\Storage\GroupContentTypeStorageInterface
   */
  protected $contentTypeStorage;

  /**
   * Constructs a new Configurator object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\group\Plugin\GroupContentEnablerManagerInterface $content_enabler_manager
   *   The group content enabler plugin manager.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    GroupContentEnablerManagerInterface $content_enabler_manager
  ) {
    $this->contentTypeStorage = $entity_type_manager
      ->getStorage('group_content_type');

    // Injections required by the `RoleConfiguratorTrait` trait.
    $this->contentEnablerManager = $content_enabler_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->groupRoleStorage = $entity_type_manager->getStorage('group_role');
  }

  /**
   * {@inheritdoc}
   */
  public function configure() {
    $this->configureDefaultRoles();
  }

  /**
   * {@inheritdoc}
   */
  public function configureOrderType(OrderTypeInterface $order_type) {
    $plugin_id = GroupContentEnabler::ORDER . ':' . $order_type->id();
    $content_types = $this->contentTypeStorage
      ->loadByContentPluginId($plugin_id);
    foreach ($content_types as $content_type) {
      if ($content_type->getGroupType()->id() !== GroupBundle::ORGANIZATION) {
        continue;
      }

      $content_type->updateContentPlugin([
        // We assume that it only makes sense that an order is placed on behalf
        // of one organization only.
        'group_cardinality' => 1,
        // We assume that it only makes sense that an order has only one group
        // content on the same organization. If that's not the case a different
        // group content enabler plugin is likely to be used for the specific
        // use case.
        'entity_cardinality' => 1,
        // We assume that the group content type does not have additional
        // fields; it is therefore not necessary to use the wizard.
        'use_creation_wizard' => 0,
      ]);
      $content_type->save();

      // Configure permissions for this order type's plugin only.
      $this->configureDefaultRoles($plugin_id);

      return;
    }

    throw new \RuntimeException(
      'Cannot configure a plugin that is not installed.'
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultRolesInfo() {
    return [
      GroupRole::ANONYMOUS => [
        'id' => GroupRole::ANONYMOUS,
        'label' => 'Anonymous',
      ],
      GroupRole::MANAGER => [
        'id' => GroupRole::MANAGER,
        'label' => 'Manager',
      ],
      GroupRole::MEMBER => [
        'id' => GroupRole::MEMBER,
        'label' => 'Member',
      ],
      GroupRole::OUTSIDER => [
        'id' => GroupRole::OUTSIDER,
        'label' => 'Outsider',
      ],
      GroupRole::PURCHASER => [
        'id' => GroupRole::PURCHASER,
        'label' => 'Purchaser',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultRolePermissions() {
    $permissions = [
      GroupRole::ANONYMOUS => [
        // Order permissions.
        'access commerceg_order overview' => FALSE,
        'create commerceg_order: entity' => FALSE,
        'delete any commerceg_order: entity' => FALSE,
        'delete own commerceg_order: entity' => FALSE,
        'update any commerceg_order: entity' => FALSE,
        'update own commerceg_order: entity' => FALSE,
        'view commerceg_order: entity' => FALSE,
        'create commerceg_order: content' => FALSE,
        'delete any commerceg_order: content' => FALSE,
        'delete own commerceg_order: content' => FALSE,
        'update any commerceg_order: content' => FALSE,
        'update own commerceg_order: content' => FALSE,
        'view commerceg_order: content' => FALSE,
        // Cart permissions.
        'view any commerceg_order: cart' => FALSE,
        'view own commerceg_order: cart' => FALSE,
        'update any commerceg_order: cart' => FALSE,
        'update own commerceg_order: cart' => FALSE,
        'checkout any commerceg_order: cart' => FALSE,
        'checkout own commerceg_order: cart' => FALSE,
      ],
      GroupRole::OUTSIDER => [
        // Order permissions.
        'access commerceg_order overview' => FALSE,
        'create commerceg_order: entity' => FALSE,
        'delete any commerceg_order: entity' => FALSE,
        'delete own commerceg_order: entity' => FALSE,
        'update any commerceg_order: entity' => FALSE,
        'update own commerceg_order: entity' => FALSE,
        'view commerceg_order: entity' => FALSE,
        'create commerceg_order: content' => FALSE,
        'delete any commerceg_order: content' => FALSE,
        'delete own commerceg_order: content' => FALSE,
        'update any commerceg_order: content' => FALSE,
        'update own commerceg_order: content' => FALSE,
        'view commerceg_order: content' => FALSE,
        // Cart permissions.
        'view any commerceg_order: cart' => FALSE,
        'view own commerceg_order: cart' => FALSE,
        'update any commerceg_order: cart' => FALSE,
        'update own commerceg_order: cart' => FALSE,
        'checkout any commerceg_order: cart' => FALSE,
        'checkout own commerceg_order: cart' => FALSE,
      ],
      GroupRole::MEMBER => [
        // Order permissions.
        'access commerceg_order overview' => TRUE,
        'create commerceg_order: entity' => FALSE,
        'delete any commerceg_order: entity' => FALSE,
        'delete own commerceg_order: entity' => FALSE,
        'update any commerceg_order: entity' => FALSE,
        'update own commerceg_order: entity' => FALSE,
        'view commerceg_order: entity' => TRUE,
        'create commerceg_order: content' => FALSE,
        'delete any commerceg_order: content' => FALSE,
        'delete own commerceg_order: content' => FALSE,
        'update any commerceg_order: content' => FALSE,
        'update own commerceg_order: content' => FALSE,
        'view commerceg_order: content' => FALSE,
        // Cart permissions.
        'view any commerceg_order: cart' => TRUE,
        'view own commerceg_order: cart' => NULL,
        'update any commerceg_order: cart' => TRUE,
        'update own commerceg_order: cart' => NULL,
        'checkout any commerceg_order: cart' => FALSE,
        'checkout own commerceg_order: cart' => FALSE,
      ],
      GroupRole::MANAGER => [
        // Order permissions.
        'access commerceg_order overview' => NULL,
        'create commerceg_order: entity' => FALSE,
        'delete any commerceg_order: entity' => FALSE,
        'delete own commerceg_order: entity' => FALSE,
        'update any commerceg_order: entity' => FALSE,
        'update own commerceg_order: entity' => FALSE,
        'view commerceg_order: entity' => NULL,
        'create commerceg_order: content' => FALSE,
        'delete any commerceg_order: content' => FALSE,
        'delete own commerceg_order: content' => FALSE,
        'update any commerceg_order: content' => FALSE,
        'update own commerceg_order: content' => FALSE,
        'view commerceg_order: content' => FALSE,
        // Cart permissions.
        'view any commerceg_order: cart' => NULL,
        'view own commerceg_order: cart' => NULL,
        'update any commerceg_order: cart' => NULL,
        'update own commerceg_order: cart' => NULL,
        'checkout any commerceg_order: cart' => FALSE,
        'checkout own commerceg_order: cart' => FALSE,
      ],
      GroupRole::PURCHASER => [
        // Group permissions.
        'access content overview' => FALSE,
        'administer group' => FALSE,
        'delete group' => FALSE,
        'edit group' => FALSE,
        'view group' => NULL,
        // Group membership permissions.
        'administer members' => FALSE,
        'update own group_membership content' => NULL,
        'join group' => FALSE,
        'leave group' => FALSE,
        'view group_membership content' => NULL,
        // Order permissions.
        'access commerceg_order overview' => NULL,
        'create commerceg_order: entity' => FALSE,
        'delete any commerceg_order: entity' => FALSE,
        'delete own commerceg_order: entity' => FALSE,
        'update any commerceg_order: entity' => FALSE,
        'update own commerceg_order: entity' => FALSE,
        'view commerceg_order: entity' => NULL,
        'create commerceg_order: content' => FALSE,
        'delete any commerceg_order: content' => FALSE,
        'delete own commerceg_order: content' => FALSE,
        'update any commerceg_order: content' => FALSE,
        'update own commerceg_order: content' => FALSE,
        'view commerceg_order: content' => FALSE,
        // Cart permissions.
        'view any commerceg_order: cart' => NULL,
        'view own commerceg_order: cart' => NULL,
        'update any commerceg_order: cart' => NULL,
        'update own commerceg_order: cart' => NULL,
        'checkout any commerceg_order: cart' => TRUE,
        'checkout own commerceg_order: cart' => NULL,
        // Child organization permissions.
        'access commerceg_organization overview' => NULL,
        'create commerceg_organization entity' => FALSE,
        'delete any commerceg_organization entity' => FALSE,
        'delete own commerceg_organization entity' => FALSE,
        'update any commerceg_organization entity' => FALSE,
        'update own commerceg_organization entity' => FALSE,
        'view commerceg_organization entity' => NULL,
        'create commerceg_organization content' => FALSE,
        'delete any commerceg_organization content' => FALSE,
        'delete own commerceg_organization content' => FALSE,
        'update any commerceg_organization content' => FALSE,
        'update own commerceg_organization content' => FALSE,
        'view commerceg_organization content' => NULL,
      ],
    ];

    return $this->buildDerivativePermissions(
      'commerce_order_type',
      GroupContentEnabler::ORDER,
      $permissions
    );
  }

}
