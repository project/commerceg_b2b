<?php

namespace Drupal\commerceg_b2b_order\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Implements callbacks for forms provided by other modules.
 *
 * Submit and validate callbacks are added here instead of module files for
 * better OOP. They are static methods as they are simple PHP callables and not
 * fetched through the service container.
 *
 * We also do not add them to the form alter hook service because mixing static
 * with non-static methods on the same class can create problems is not a good
 * practice; it causes issues with automated tests.
 *
 * @I Consider using a render element
 *    type     : task
 *    priority : low
 *    labels   : coding-standards, order
 */
class Callbacks {

  /**
   * Validates B2B integration settings for the submitted order type.
   *
   * @param array $form
   *   The form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   */
  public static function orderTypeValidate(
    array $form,
    FormStateInterface $form_state
  ) {
    $order_type = $form_state->getFormObject()->getEntity();
    $values = $form_state->getValues();

    $installer = \Drupal::service('commerceg_b2b_order.installer');
    $is_installed = $installer->isOrderTypeInstalled($order_type);

    $can_uninstall = NULL;
    if ($is_installed) {
      $can_uninstall = $installer->canUninstallOrderType($order_type);
    }

    if (!$values['commerceg_b2b_status'] && $is_installed && !$can_uninstall) {
      $form_state->setErrorByName(
        'commerceg_b2b_status',
        t(
          'You cannot remove availability of this order type from Organizations
           because there already exist orders or carts of this type that belong
           to Organizations.'
        )
      );
    }
  }

  /**
   * Installs/uninstalls B2B integration for the submitted order type.
   *
   * @param array $form
   *   The form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   */
  public static function orderTypeSubmit(
    array $form,
    FormStateInterface $form_state
  ) {
    $messenger = \Drupal::service('messenger');
    $installer = \Drupal::service('commerceg_b2b_order.installer');
    $configurator = \Drupal::service('commerceg_b2b_order.configurator');

    $order_type = $form_state->getFormObject()->getEntity();
    $values = $form_state->getValues();

    $is_installed = $installer->isOrderTypeInstalled($order_type);

    if ($values['commerceg_b2b_status'] && !$is_installed) {
      $installer->installOrderType($order_type);
      $configurator->configureOrderType($order_type);
      $messenger->addStatus(t(
        'The %order_type order type has been successfully installed on B2B
         Organizations.',
        ['%order_type' => $order_type->label()]
      ));
    }
    elseif (!$values['commerceg_b2b_status'] && $is_installed) {
      $installer->uninstallOrderType($order_type);
      $messenger->addStatus(t(
        'The %order_type order type has been successfully uninstalled from B2B
         Organizations.',
        ['%order_type' => $order_type->label()]
      ));
    }
  }

}
